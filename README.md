# JSON to HTML

Given a JSON file/object, construct a DOM/web page, making parent-child relations in the dom tree. Every element should be draggable.

## ASSUMPTIONS
input JSON will be in format


 {"user": { "documents": {"interview": { "children" : [{"bounds": {
							"x": 7,
							"y": 10,
							"width": 799,
							"height": 448
						},
						"fill": "#c7c5f0",
						"stroke": "#707070",
						"strokeWidth": 1
					},.......]}}}}

[Sample.JSON](https://gitlab.com/viplov1994/json-to-html/-/blob/master/Sample.json)

					
## SETUP
- Extract the zip in any folder.
- Open adobe.html
- Input the desired JSON and click submit.

## Approach
I decided to make a tree with all parent and child relations, and then render it in a div in the HTML.  For this, I took the following steps
- Sorted the children array in JSON with x coordinates, y coordinates, and area of the bound. I did this to make sure that all the child elements should come after the parent. 
- I started to insert each element/object of the array in the tree ( similar to the DOM tree).
- I rendered the tree in the HTML maintaining the same hierarchy and added the attributes for styles.
- Added the interaction function of dragging, hover and select.


## FUTURE WORK
- Moving parent element without moving child element while dragging
- Handling the cases where a smaller rectangle is hidden under a bigger rectangle ( z-index of the smaller rectangle is less than the bigger rectangle). In the current approach, the smaller rectangle will come above the bigger rectangle.
